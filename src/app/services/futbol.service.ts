import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable,  Subject } from 'rxjs';
import { Router } from '@angular/router';

export interface Usuario {
  logeado: boolean;
  idUser: String;
  nombre: String;
}

@Injectable({
  providedIn: 'root'
})
export class FutbolService {

  // Esta api solo te da 100 llamadas gratis al día, luego te cobra, por eso me dessuscrbi, pero si le mete otro aki key todo funcionaria.
  private static url = "https://api-football-v1.p.rapidapi.com/";

  private userObservable$ = new Subject<Usuario>();

  constructor(private http: HttpClient, private navegacion: Router) { }

  headerLl(): HttpHeaders { return new HttpHeaders().set('x-rapidapi-key','6b45ec0df9a86bp1239945b4cd8716amsh7f0aba65b2').set('x-rapidapi-host','api-football-v1.p.rapidapi.com'); }

  nuevoUsuario(logeadoP: boolean, idUserP: string, nombreP: string): Usuario {
    return {
      logeado: logeadoP,
      idUser: idUserP,
      nombre: nombreP,
    };
  }

  tiempoSesion() {

    let tiempo = new Date();
    let newHora = tiempo.getHours() + 2;
    tiempo.setHours(newHora);
    localStorage.setItem('expiraRD', tiempo.getTime().toString() );
    
  }

  peticionGet(parametros: string) {

    const cabeceras = {headers: this.headerLl()};
    return this.http.get(FutbolService.url+parametros, cabeceras);

  }

  iniciarSesion(usuario: string, contrasenha: string) {

      if((usuario == "carlos@mail.com") && (contrasenha == "renaido")){
          
        this.setDatosUser("22", "Carlos Rodriguez Rodriguez", "true");
        this.crearObservableUser$();
        this.navegacion.navigate(['seleccionar-liga']);

      } else {
        return "err";
      }
  }

  cerrarSesion() {
    
    localStorage.clear();

    let userObservableAUX: Usuario;
    userObservableAUX = this.nuevoUsuario(false, "", "");
    this.userObservable$.next(userObservableAUX);
    this.navegacion.navigate(['iniciar-sesion']);
    
  }
    
  private setDatosUser( id: string, name: string, logeado: string) {
  
    localStorage.setItem('idUserRD', id);
    localStorage.setItem('nombreRD', name);
    localStorage.setItem('logeado', logeado);

  }

  getDatosUser(dato: string): string {

    if ( localStorage.getItem(dato) ) {
      return localStorage.getItem(dato);
    } else {
      return "";
    }
  }

  estaLogeado(): boolean {

    let aux = localStorage.getItem('expiraRD');
    if( aux === null ) { return false; }

    let expira = parseInt(localStorage.getItem('expiraRD'));
    let expiraDate = new Date();
    expiraDate.setTime(expira);

    if ( expiraDate > new Date() ) {
      this.tiempoSesion();
      return true;
    } else {
      return false;
    }
  }
  
  crearObservableUser$() {

    let userObservableAUX: Usuario;
    userObservableAUX = this.nuevoUsuario(true, this.getDatosUser("idUserRD"), this.getDatosUser("nombreRD"));

    this.userObservable$.next(userObservableAUX);

  }

  public getObservableUser$(): Observable<Usuario> {

    return this.userObservable$.asObservable();

  }

  comprobarLogeoObservable$() {
    
    if (this.estaLogeado()) {
      this.crearObservableUser$();
    } else {
      let userObservableAUX: Usuario;
      userObservableAUX = this.nuevoUsuario(false, "", "");
      this.userObservable$.next(userObservableAUX);
    }

  }

  seleccionarLiga(liga: any){
    localStorage.setItem('ligaID', liga);
    this.peticionGet("v3/leagues?id="+liga)
      .subscribe( ( data: any) =>  {
        localStorage.setItem('ligaNom', data.response[0].league.name);
      });
      this.navegacion.navigate(['home/listado-equipos']);  
  }

}
