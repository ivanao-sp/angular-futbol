import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { FilterModule } from '@josee9988/filter-pipe-ngx';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ListadoEquiposComponent } from './components/listado-equipos/listado-equipos.component';
import { JugadoresComponent } from './components/jugadores/jugadores.component';
import { EventosComponent } from './components/eventos/eventos.component';
import { IniciarSesionComponent } from './components/iniciar-sesion/iniciar-sesion.component';
import { SeleccionarLigaComponent } from './components/seleccionar-liga/seleccionar-liga.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HeaderComponent,
    ListadoEquiposComponent,
    JugadoresComponent,
    EventosComponent,
    IniciarSesionComponent,
    SeleccionarLigaComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FilterModule,
    AppRoutingModule,
    FormsModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
