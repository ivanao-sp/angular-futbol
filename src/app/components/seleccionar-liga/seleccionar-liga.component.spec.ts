import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeleccionarLigaComponent } from './seleccionar-liga.component';

describe('SeleccionarLigaComponent', () => {
  let component: SeleccionarLigaComponent;
  let fixture: ComponentFixture<SeleccionarLigaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeleccionarLigaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeleccionarLigaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
