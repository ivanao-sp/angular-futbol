import { Component, OnInit } from '@angular/core';
import { FutbolService } from '../../services/futbol.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-seleccionar-liga',
  templateUrl: './seleccionar-liga.component.html',
  styleUrls: ['./seleccionar-liga.component.css']
})
export class SeleccionarLigaComponent implements OnInit {

  ligas: any;
  ligaSelect: number;

  constructor(private futbol: FutbolService, private navegacion: Router) {

    if(futbol.getDatosUser('logeado') == ''){
      this.navegacion.navigate(['iniciar-sesion']);
    }
    
    this.futbol.peticionGet("v3/leagues?season=2020&last=10")
      .subscribe( ( data: any) =>  {
        this.ligas = data.response;
        console.log(this.ligas);
      });
  }

  ngOnInit(): void {
    
  }

  seleccionarLiga(){
    this.futbol.seleccionarLiga(this.ligaSelect);
  }

}
