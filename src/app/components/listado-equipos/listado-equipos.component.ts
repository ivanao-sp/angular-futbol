import { Component, OnInit } from '@angular/core';
import { FutbolService } from '../../services/futbol.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listado-equipos',
  templateUrl: './listado-equipos.component.html',
  styleUrls: ['./listado-equipos.component.css']
})
export class ListadoEquiposComponent implements OnInit {

  listadoEquipos: any;
  jugadoresEquipo: any;
  filtrarEquipos: string = '';
  ocultarJugadores = true;
  idEquipoSelect = null;
  liga: string;

  public contact = { name: '' };


  constructor( private futbol: FutbolService, private navegacion: Router ) {

    if(futbol.getDatosUser('logeado') == ''){
      this.navegacion.navigate(['iniciar-sesion']);
    }

    this.futbol.peticionGet("v2/teams/league/"+futbol.getDatosUser('ligaID'))
      .subscribe( ( data: any) =>  {
        this.listadoEquipos = data.api.teams;
        console.log(data);
      });

  }

  ngOnInit(): void {
  }

  equipoSelectPrimer(id: number) { // esta función pone el aquipo seleccionado de primero en la lista

    let aux: any;
    for (let i = 0; i < this.listadoEquipos.length; i++) { // utilizo un for y no un forEach para poder romper el bucle
      if (this.listadoEquipos[i].team_id === id) {
        aux = this.listadoEquipos[i];
        this.listadoEquipos.splice(i, 1);
        break;
      } 
    }
    this.listadoEquipos.unshift(aux);

  }

  traerJugadores(idEquipo: number) {

    this.futbol.peticionGet("v3/players?team="+idEquipo+"&season=2020")
      .subscribe( ( data: any) =>  {
        this.jugadoresEquipo = data.response;
        console.log(this.jugadoresEquipo);
    });

    this.ocultarJugadores = false;
    this.equipoSelectPrimer(idEquipo);
    this.idEquipoSelect = idEquipo;

  }

  cerrarJugadores(d){
    this.ocultarJugadores = true;
  }

}
