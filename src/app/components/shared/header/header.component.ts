import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ViewChild } from '@angular/core';
import { FutbolService, Usuario } from '../../../services/futbol.service';
import { Observable,  Subject } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [NgbModalConfig, NgbModal]
})
export class HeaderComponent implements OnInit {

  datos: Observable<Usuario>;

  constructor(config: NgbModalConfig, private modalService: NgbModal, public futbol: FutbolService) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit(): void {

  }

}
