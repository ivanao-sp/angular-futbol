import { Component, OnInit } from '@angular/core';
import { FutbolService } from '../../services/futbol.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.component.html',
  styleUrls: ['./eventos.component.css']
})
export class EventosComponent implements OnInit {

  listadoEventos: any;
  filtrarEventos = '';
  clases = ['bg-primary', 'bg-secondary', 'bg-success', 'bg-danger', 'bg-warning', 'bg-info', 'bg-dark'];

  constructor( private futbol: FutbolService, private navegacion: Router ) {

    if(futbol.getDatosUser('logeado') == ''){
      this.navegacion.navigate(['iniciar-sesion']);
    }

    this.futbol.peticionGet("v2/events/214226")
      .subscribe( ( data: any) =>  {
        this.listadoEventos = data.api.events;
        this.asignarClases()
      });

  }

  ngOnInit(): void {
  }

  asignarClases(){

    let contador = 0;
    this.listadoEventos.forEach( function(valor, indice, array) {
      valor["clase"] = contador;
      contador++;
      if(contador==7){contador = 0;}
    });

  }

}
