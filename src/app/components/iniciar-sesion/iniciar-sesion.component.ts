import { Component, OnInit } from '@angular/core';
import { FutbolService } from '../../services/futbol.service';

@Component({
  selector: 'app-iniciar-sesion',
  templateUrl: './iniciar-sesion.component.html',
  styleUrls: ['./iniciar-sesion.component.css']
})
export class IniciarSesionComponent implements OnInit {
  
  correo = '';
  pass = '';
  error = false;

  constructor(private futbol: FutbolService) { }

  ngOnInit(): void {
  }

  iniciaSesion(){
      if(this.futbol.iniciarSesion(this.correo, this.pass)=="err"){
        this.error = true;
      }
  }

}
