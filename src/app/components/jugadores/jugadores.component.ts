import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-jugadores',
  templateUrl: './jugadores.component.html',
  styleUrls: ['./jugadores.component.css']
})
export class JugadoresComponent implements OnInit {

  @Input() jugadores: any[] = [];

  @Output() propagar = new EventEmitter<boolean>();

  filtrarJugadores = '';

  constructor() {
    this.propagar = new EventEmitter();
  }

  ngOnInit(): void {
  }

  cerrar(){
    this.propagar.emit(true);
  }

}
