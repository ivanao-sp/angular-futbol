import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IniciarSesionComponent } from './components/iniciar-sesion/iniciar-sesion.component';
import { SeleccionarLigaComponent } from './components/seleccionar-liga/seleccionar-liga.component';
import { ListadoEquiposComponent } from './components/listado-equipos/listado-equipos.component';
import { EventosComponent } from './components/eventos/eventos.component';


const routes: Routes = [
  { path: 'iniciar-sesion', component: IniciarSesionComponent },
  { path: 'seleccionar-liga', component: SeleccionarLigaComponent },
  { path: 'home/listado-equipos', component: ListadoEquiposComponent },
  { path: 'eventos', component: EventosComponent },
  { path: '**',  pathMatch: 'full', redirectTo: 'home/listado-equipos'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
