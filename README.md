# AngularFutbol

Proyecto de prueba en Angular + ng-boostrap.

Tiene un logeo fake -> mail: "carlos@mail.com" pass: "renaido".

Hace llamadas contra la api https://api-football-v1.p.rapidapi.com para obtener datos de equipos, jugadores y eventos. (algunos equipos no tienen jugadores).

Para poder probar las llamadas hay que registrarse en la api e introducir un número de cuenta para obtener un api-key con suscripción (api-key sin suscripción no funciona). Esto nos da 100 llamadas diarias gratuitas y luego cada llamada cuesta 0,00450€.
Es por esa razón que el api-key actual fue desactivado.

No se le ha hecho la responsividad de CSS (por ser solo un proyecto de prueba) por lo que debe probarse en una pantalla grande.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
